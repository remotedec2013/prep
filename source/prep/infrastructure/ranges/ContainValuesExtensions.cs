﻿using System;

namespace prep.infrastructure.ranges
{
    public static class ContainValuesExtensions
    {
        public static IContainValues<T> intersection<T>(this IContainValues<T> left, IContainValues<T> right) where T : IComparable<T>
        {
            return new IntersectionRange<T>(left, right);
        }

        public static IContainValues<T> union<T>(this IContainValues<T> left, IContainValues<T> right) where T : IComparable<T>
        {
            return new UnionRange<T>(left, right);
        }

        public static IContainValues<T> invert<T>(this IContainValues<T> range) where T : IComparable<T>
        {
            return new InvertRange<T>(range);
        }
    }
}

