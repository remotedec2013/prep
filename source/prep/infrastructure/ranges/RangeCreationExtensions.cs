﻿using System;

namespace prep.infrastructure.ranges
{
    public static class RangeCreationExtensions
    {
        public static FromRange<T> from<T>(this RangeCreationExtensionPoint<T> extension_point, T value) where T : IComparable<T>
        {
            return new FromRange<T>(value);
        }

        public static ToRange<T> to<T>(this RangeCreationExtensionPoint<T> extension_point, T value) where T : IComparable<T>
        {
            return new ToRange<T>(value);
        }
    }
}
