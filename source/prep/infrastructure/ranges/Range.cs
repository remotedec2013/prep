﻿using System;

namespace prep.infrastructure.ranges
{
  public static class AlternateRange
  {
    public static RangeCreationExtensionPoint<T> from<T>(T value) where T : IComparable<T>
    {
      return new RangeCreationExtensionPoint<T>(value);
    }
  }

  public static class Range<T> where T : IComparable<T>
  {
    public static RangeCreationExtensionPoint<T> range
    {
      get { return AlternateRange.from(default(T));}
    }
  }

  public static class ComparableRangeExtensions
  {
    public static RangeCreationExtensionPoint<T> as_range<T>(this T value) where T : IComparable<T>
    {
      return AlternateRange.from(value);
    }
  }
}