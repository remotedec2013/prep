﻿using System;

namespace prep.infrastructure.ranges
{
    public class FromRange<T> : IContainValues<T> where T : IComparable<T>
    {
        private T from_value;

        public FromRange(T from_value)
        {
            this.from_value = from_value;
        }

        public FromRangeInclusive inclusive
        {
            get { return new FromRangeInclusive(from_value); }
        }

        public FromToRange<T> to(T value)
        {
            // Allow the syntax - Range<T>.from_value(x).to(y)
            return new FromToRange<T>(this, value, false);
        }

        public bool contains(T value)
        {
            return value.CompareTo(from_value) > 0;
        }

        public class FromRangeInclusive : IContainValues<T> 
        {
            private T from;

            public FromRangeInclusive(T from)
            {
                this.from = from;
            }

            public FromToRange<T> to(T value)
            {
                return new FromToRange<T>(this, value, false);
            }

            public bool contains(T value)
            {
                return value.CompareTo(from) >= 0;
            }
        }
    }
}
