﻿using System;

namespace prep.infrastructure.ranges
{
    public class ToRange<T> : IContainValues<T> where T : IComparable<T>
    {
        private T to_value;

        public ToRange(T to_value)
        {
            this.to_value = to_value;
        }

        public IContainValues<T> inclusive
        {
            get { return new ToRangeInclusive(to_value); }
        }

        public bool contains(T value)
        {
            return value.CompareTo(to_value) < 0;
        }

        public class ToRangeInclusive : IContainValues<T> 
        {
            private T to_value;

            public ToRangeInclusive(T to_value)
            {
                this.to_value = to_value;
            }

            public bool contains(T value)
            {
                return value.CompareTo(to_value) <= 0;
            }
        }
    }
}
