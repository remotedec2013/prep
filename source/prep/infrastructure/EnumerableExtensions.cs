﻿using System;
using System.Collections.Generic;
using prep.infrastructure.comparing;
using prep.infrastructure.filtering;

namespace prep.infrastructure
{
  public static class EnumerableExtensions
  {
    public static IEnumerable<T> one_at_a_time<T>(this IEnumerable<T> items)
    {
      foreach (var item in items) yield return item;
    }

    public static IEnumerable<ItemToMatch> all_items_matching<ItemToMatch>(this IEnumerable<ItemToMatch> items,
                                                                           Condition<ItemToMatch> condition)
    {
      foreach (var item in items)
      {
        if (condition(item)) yield return item;
      }
    }

    public static IEnumerable<ItemToMatch> all_items_matching<ItemToMatch>(this IEnumerable<ItemToMatch> items,
                                                                           IMatchA<ItemToMatch> matcher)
    {
      return items.all_items_matching(matcher.matches);
    }

    public static IEnumerable<ItemToSort> sort_using<ItemToSort>(this IEnumerable<ItemToSort> items,
                                                                 IComparer<ItemToSort> comparison)
    {
      var result = new List<ItemToSort>(items);
      result.Sort(comparison);
      return result;
    }

    public static IEnumerable<ItemToMatch> sort_by<ItemToMatch, PropertyType>(this IEnumerable<ItemToMatch> items,
                                                                              PropertyAccessor
                                                                                <ItemToMatch, PropertyType> accessor) where PropertyType : IComparable<PropertyType>
    {
      return items.sort_using(Compare<ItemToMatch>.by(accessor));
    }

    public static IEnumerable<ItemToMatch> sort_by_descending<ItemToMatch, PropertyType>(
      this IEnumerable<ItemToMatch> items,
      PropertyAccessor<ItemToMatch, PropertyType> accessor) where PropertyType : IComparable<PropertyType>
    {
      return items.sort_using(Compare<ItemToMatch>.by_descending(accessor));
    }
  }
}