﻿namespace prep.infrastructure.filtering
{
  public class MatchCreationExtensionPoint<ItemToMatch, PropertyType> :
    IProvideAccessToMatchCreationExtensions<ItemToMatch, PropertyType>
  {
    PropertyAccessor<ItemToMatch, PropertyType> accessor;

    public IProvideAccessToMatchCreationExtensions<ItemToMatch, PropertyType> not
    {
      get { return new NegatedMatchCreationExtensionPoint(this); }
    }

    class NegatedMatchCreationExtensionPoint : IProvideAccessToMatchCreationExtensions<ItemToMatch, PropertyType>
    {
      IProvideAccessToMatchCreationExtensions<ItemToMatch, PropertyType> original;

      public NegatedMatchCreationExtensionPoint(
        IProvideAccessToMatchCreationExtensions<ItemToMatch, PropertyType> original)
      {
        this.original = original;
      }

      public IMatchA<ItemToMatch> create_matcher(IMatchA<PropertyType> value_matcher)
      {
        return original.create_matcher(value_matcher).not();
      }
    }

    public MatchCreationExtensionPoint(PropertyAccessor<ItemToMatch, PropertyType> accessor)
    {
      this.accessor = accessor;
    }

    public IMatchA<ItemToMatch> create_matcher(IMatchA<PropertyType> value_matcher)
    {
      return new PropertyMatch<ItemToMatch, PropertyType>(accessor, value_matcher);
    }
  }
}