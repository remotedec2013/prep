﻿namespace prep.infrastructure.filtering
{
  public class PropertyMatch<ItemToMatch, PropertyType> :IMatchA<ItemToMatch>
  {
    PropertyAccessor<ItemToMatch, PropertyType> accessor;
    IMatchA<PropertyType> value_specification;

    public PropertyMatch(PropertyAccessor<ItemToMatch, PropertyType> accessor, IMatchA<PropertyType> value_specification)
    {
      this.accessor = accessor;
      this.value_specification = value_specification;
    }

    public bool matches(ItemToMatch item)
    {
      var value = accessor(item);
      return value_specification.matches(value);
    }
  }
}