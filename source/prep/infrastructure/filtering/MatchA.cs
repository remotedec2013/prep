﻿using System.Collections.Generic;

namespace prep.infrastructure.filtering
{
  public class MatchA<ItemToMatch>
  {
    public static MatchCreationExtensionPoint<ItemToMatch, PropertyType> having_a<PropertyType>(
      PropertyAccessor<ItemToMatch, PropertyType> accessor)
    {
      return new MatchCreationExtensionPoint<ItemToMatch, PropertyType>(accessor);
    }
  }

}