﻿using System;
using prep.infrastructure.ranges;

namespace prep.infrastructure.filtering
{
  public static class ComparableMatchCreationExtensions
  {
    public static IMatchA<ItemToMatch> falls_in<ItemToMatch, PropertyType>(
      this MatchCreationExtensionPoint<ItemToMatch, PropertyType> extension_point, IContainValues<PropertyType> range)
      where PropertyType : IComparable<PropertyType>
    {
      return extension_point.create_with_value_matcher(new FallsInRange<PropertyType>(range));
    }

    public static IMatchA<ItemToMatch> greater_than<ItemToMatch, PropertyType>(
      this MatchCreationExtensionPoint<ItemToMatch, PropertyType> extension_point, PropertyType value)
      where PropertyType : IComparable<PropertyType>
    {
      return extension_point.falls_in(value.as_range());
    }

    public static IMatchA<ItemToMatch> between<ItemToMatch, PropertyType>(
      this MatchCreationExtensionPoint<ItemToMatch, PropertyType> extension_point, PropertyType start, PropertyType end)
      where PropertyType : IComparable<PropertyType>
    {
      //Implement the creation of the range using your own design for a dsl that can be used to create ranges of any
      //type that implements IComparable<PropertyType>
      return extension_point.falls_in(start.as_range().to(end).inclusive);
    }
  }
}