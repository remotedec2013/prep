﻿namespace prep.infrastructure.filtering
{
  public static class MatchCreationExtensions
  {
    public static IMatchA<ItemToMatch> equal_to<ItemToMatch, PropertyType>(
      this IProvideAccessToMatchCreationExtensions<ItemToMatch, PropertyType> extension_point, PropertyType value)
    {
      return equal_to_any(extension_point, value);
    }

    public static IMatchA<ItemToMatch> equal_to_any<ItemToMatch, PropertyType>(
      this IProvideAccessToMatchCreationExtensions<ItemToMatch, PropertyType> extension_point, params PropertyType[] values)
    {
      return create_with_value_matcher(extension_point, new EqualToAny<PropertyType>(values));
    }

    public static IMatchA<ItemToMatch> create_from<ItemToMatch, PropertyType>(
      this IProvideAccessToMatchCreationExtensions<ItemToMatch, PropertyType> extension_point, Condition<ItemToMatch> condition)
    {
      return new AnonymousMatch<ItemToMatch>(condition);
    }

    public static IMatchA<ItemToMatch> create_with_value_matcher<ItemToMatch, PropertyType>(
      this IProvideAccessToMatchCreationExtensions<ItemToMatch, PropertyType> extension_point, IMatchA<PropertyType> value_matcher)
    {
      return extension_point.create_matcher(value_matcher);
    }
  }
}
