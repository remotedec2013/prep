﻿namespace prep.infrastructure.filtering
{
  public delegate PropertyType PropertyAccessor<ItemToRetrievePropertyFrom, PropertyType>(ItemToRetrievePropertyFrom item);
}