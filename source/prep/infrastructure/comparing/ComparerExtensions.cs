﻿using System;
using System.Collections.Generic;
using prep.infrastructure.filtering;

namespace prep.infrastructure.comparing
{
  public static class ComparerExtensions
  {
    public static IComparer<ItemToCompare> then_by_descending<ItemToCompare,PropertyToCompare>(this IComparer<ItemToCompare> first, 
      PropertyAccessor<ItemToCompare, PropertyToCompare> accessor)
      where PropertyToCompare : IComparable<PropertyToCompare>
    {
      return first.then_by(Compare<ItemToCompare>.by_descending(accessor));
    }

    public static IComparer<ItemToCompare> then_by<ItemToCompare,PropertyToCompare>(this IComparer<ItemToCompare> first,
      PropertyAccessor<ItemToCompare,PropertyToCompare> accessor) 
      where PropertyToCompare : IComparable<PropertyToCompare>
    {
      return first.then_by(Compare<ItemToCompare>.by(accessor));
    }

    public static IComparer<ItemToCompare> then_by<ItemToCompare,PropertyToCompare>(this IComparer<ItemToCompare> first, PropertyAccessor<ItemToCompare,PropertyToCompare> accessor,
                                                                                    params PropertyToCompare[] fixed_order) 
    {
      return first.then_by(Compare<ItemToCompare>.by(accessor, fixed_order));
    }


    public static IComparer<ItemToCompare> then_by<ItemToCompare>(this IComparer<ItemToCompare> first,
      IComparer<ItemToCompare> second)
    {
      return new CombinedComparer<ItemToCompare>(first, second);
    }

    public static IComparer<ItemToCompare> reverse<ItemToCompare>(this IComparer<ItemToCompare> to_reverse)
    {
      return new ReverseComparer<ItemToCompare>(to_reverse);
    }
  }
}
