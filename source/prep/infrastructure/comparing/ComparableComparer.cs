﻿using System;
using System.Collections.Generic;

namespace prep.infrastructure.comparing
{
  public class ComparableComparer<PropertyToCompare> : IComparer<PropertyToCompare>
    where PropertyToCompare : IComparable<PropertyToCompare>
  {
    public int Compare(PropertyToCompare x, PropertyToCompare y)
    {
      return x.CompareTo(y);
    }
  }
}