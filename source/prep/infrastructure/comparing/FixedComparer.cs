﻿using System.Collections.Generic;

namespace prep.infrastructure.comparing
{
  public class FixedComparer<PropertyToCompare> : IComparer<PropertyToCompare>
  {
    IList<PropertyToCompare> fixed_order;

    public FixedComparer(params PropertyToCompare[] fixed_order)
    {
      this.fixed_order = new List<PropertyToCompare>(fixed_order);
    }

    public int Compare(PropertyToCompare x, PropertyToCompare y)
    {
      return fixed_order.IndexOf(x).CompareTo(fixed_order.IndexOf(y));
    }
  }
}