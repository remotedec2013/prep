﻿using System.Collections.Generic;
using prep.infrastructure.filtering;

namespace prep.infrastructure.comparing
{
  public class PropertyComparer<ItemToCompare, PropertyToCompare> : IComparer<ItemToCompare>
  {
    PropertyAccessor<ItemToCompare, PropertyToCompare> accessor;
    IComparer<PropertyToCompare> value_comparer;

    public PropertyComparer(PropertyAccessor<ItemToCompare, PropertyToCompare> accessor,
                            IComparer<PropertyToCompare> value_comparer)
    {
      this.accessor = accessor;
      this.value_comparer = value_comparer;
    }

    public int Compare(ItemToCompare x, ItemToCompare y)
    {
      var value1 = accessor(x);
      var value2 = accessor(y);

      return value_comparer.Compare(value1, value2);
    }
  }
}