#Design Principles


1. Code to contracts not implementations
2. TDA (Tell don't ask)
3. Highlander Principle - There can be only one
  * The one with the most ones (arguments)
  * The one with the most specific ones (arguments)
4. Open Closed Principle (Stay open for extension but closed for modification)
5. Single Responsibility Principle (SRP) - High Cohesion
6. Encapsulate what varies
7. DRY (Don't repeat yourself)
8. Favour composition over inheritance
